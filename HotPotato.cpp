#include<iostream>
#include<list>
using namespace std;
int main() {
	list<int> game; //create int list that name is game 
	int N; // create variable N for amount of people
	int M; // create variable M for amount of passes
	cout << "How many people? : "; //input amount of people
	cin >> N;
	cout << "How many step of passes? : "; //input amount of passes
	cin >> M;
	for(int i=1;i<=N;i++) //add each people into the game
	{
		game.push_back(i);
	}
	list<int>::iterator it = game.begin(); //create iterator name it to point the element of list
	list<int>::iterator tmp; //create iterator name tmp to point the element that has hot-potato
	while(game.size()>1)
	{
		for(int i=1;i<=M;i++) //loop of passing the hot-potato
		{
			if(*it==game.back()) //if this element is the last of list ,then next-moving iterator must skip back to the fist element
			{
				it = game.begin();
			}
			
			else //else just normally move the iterator
			{
				it++;
			}
		}
		tmp = it;//tmp keep(point) the element that has the hot-potato
		if (*it == game.back()) //move iterator one step
		{
			it = game.begin();
		}
		else
		{
			it++;
		}
		game.erase(tmp);//eliminate element that has hot-potato 
	}
	cout << "The winner is "; //show the last element that still alive.
	cout << "\"" << *it << "\"" << endl;
	system("pause");
	return 0;
}

